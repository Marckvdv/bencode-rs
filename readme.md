# Bencode-rs

Simple library for parsing *bencode*-encoded data (such as .torrent files). Altough I haven't found any problems yet, usage is **not** recommended because I haven't tested it all that much. It doesn't use rust's `Encodable` and `Decodable` trait yet, as I haven't looked into those yet.

## Usage

Add this repo to your 'Cargo.toml' file and you're ready to go. The library as of now only exports two things, the `decode` function:

	pub fn decode(code: &[u8]) -> Option<Vec<BItem>>;

And: 

	pub enum BItem {
	    Integer(i64),
	    Bytes(Vec<u8>),     // Byte string, need not be valid UTF-8
	    List(Vec<BItem>),
	    Dict(Vec<(Vec<u8>, BItem)>),
	    None
	}

`decode` returns all the parsed in `code`, or `None` if it failed. The parsed data wil be stored in the `BItem` enum.

## Examples

Here is a example on how to use the library:

	extern crate bencode;

	use bencode::decode;
	use bencode::BItem;

	fn main() {
		assert_eq!(decode(b"d2:idi1e4:name3:joee"),
		Some(
			vec![BItem::Dict(
				vec![
					(b"id".to_vec(), BItem::Integer(1)),
					(b"name".to_vec(), BItem::Bytes(b"joe".to_vec()))
				])
			]
		));
	}

## Utilities

The library also supports searching for keys in dictionaries:

	let parsed = decode(b"d2:idi1e4:name3:joee");
	let dict_content = match parsed {
		BItem::Dict(d) => d,
	_ => panic!("Item is not a dictionary");
	};

	find(dict_content, b"id");	// => Some(&BItem::Integer(1))
