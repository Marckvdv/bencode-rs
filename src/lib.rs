enum Type {
    Integer,
    Bytes,
    List,
    Dict,
    None
}

#[deriving(PartialEq, Show)]
pub enum BItem {
    Integer(i64),
    Bytes(Vec<u8>),     // Byte string, need not be valid UTF-8
    List(Vec<BItem>),
    Dict(Vec<(Vec<u8>, BItem)>),
    None
}

pub fn find<'a>(dict: &'a[(Vec<u8>, BItem)], key: &[u8]) -> Option<&'a BItem> {
    let result: Option<&(Vec<u8>, BItem)> = dict.iter().find(|&&(ref k, _)| k.as_slice() == key);
    match result {
        Some(&(_, ref v)) => Some(v),
        None => None
    }
}

pub fn decode(code: &[u8]) -> Option<Vec<BItem>> {
    if code.len() == 0 { return None; }

    let mut items = Vec::new();
    let mut rest = code;

    loop {
        let (result, consumed) = parse_current(rest);
        if result == BItem::None { return None; }

        items.push(result);

        if consumed == rest.len() { break; }
        rest = rest.slice_from(consumed);
    }

    Some(items)
}

fn predict_type(first_byte: u8) -> Type {
    match first_byte as char {
        'i' => Type::Integer,
        'd' => Type::Dict,
        'l' => Type::List,
        '0' ... '9' => Type::Bytes,
        _ => Type::None
    }
}

fn parse_current(code: &[u8]) -> (BItem, uint) {
    let b_type = predict_type(code[0]);
    match b_type {
        Type::Integer => parse_integer(code),
        Type::Dict => parse_dict(code),
        Type::List => parse_list(code),
        Type::Bytes => parse_bytes(code),
        Type::None => (BItem::None, 0)
    }
}

// Returns a tuple containing the parsed item and the amount of bytes 'consumed'
fn parse_integer(code: &[u8]) -> (BItem, uint) {
    assert_eq!(*code.head().unwrap(), 'i' as u8);

    let end = match code.iter().position(|&u| u == 'e' as u8) {
        Some(end) => end,
        None => return (BItem::None, 0)
    };
    let str_part = code.slice(1, end as uint);

    let converted: i64 = match from_str(String::from_utf8_lossy(str_part).deref().as_slice()) {
        Some(c) => c,
        None => return (BItem::None, 0)
    };

    (BItem::Integer(converted), end as uint + 1)
}

fn parse_bytes(code: &[u8]) -> (BItem, uint) {
    let end = match code.iter().position(|&u| u == ':' as u8) {
        Some(end) => end,
        None => return (BItem::None, 0)
    };
    let str_part = code.slice_to(end as uint);

    let converted: uint = match from_str(String::from_utf8_lossy(str_part).deref().as_slice()) {
        Some(c) => c,
        None => return (BItem::None, 0)
    };
    let rest = code.slice_from(end + 1);
    if converted > rest.len() { return (BItem::None, 0); }

    let mut data = Vec::new();
    for i in rest.iter().take(converted as uint) {
        data.push(*i);
    }

    (BItem::Bytes(data), end + converted + 1)
}

fn parse_list(code: &[u8]) -> (BItem, uint) {
    assert_eq!(*code.head().unwrap(), 'l' as u8);
    let mut items = Vec::new();
    let mut rest = code.slice_from(1);
    let mut total_consumed = 1;

    loop {
        if *rest.head().unwrap() == ('e' as u8) { break; }

        let (result, consumed) = parse_current(rest);
        if result == BItem::None { return (BItem::None, 0); }
        
        items.push(result);

        rest = rest.slice_from(consumed);
        total_consumed += consumed;
    }
    total_consumed += 1;

    (BItem::List(items), total_consumed)
}

fn parse_dict(code: &[u8]) -> (BItem, uint) {
    assert_eq!(*code.head().unwrap(), 'd' as u8);

    let mut rest = code.slice_from(1);
    let mut items = Vec::new();
    let mut total_consumed = 1;

    loop {
        let (key, key_len) = parse_current(rest);
        if key == BItem::None || key_len == rest.len() { return (BItem::None, 0); }
        rest = rest.slice_from(key_len);

        let (val, val_len) = parse_current(rest);
        if val == BItem::None || val_len == rest.len() { return (BItem::None, 0); }
        rest = rest.slice_from(val_len);

        total_consumed += key_len + val_len;

        match key {
            BItem::Bytes(b) => items.push((b, val)),
            _ => return (BItem::None, 0)
        }

        if *rest.head().unwrap() == ('e' as u8) { break; }
    }
    total_consumed += 1;

    (BItem::Dict(items), total_consumed)
}

#[test]
fn test() {
    assert_eq!(parse_current(b"i123e blabla"), (BItem::Integer(123), 5));
    assert_eq!(parse_current(b"i-12e blabla"), (BItem::Integer(-12), 5));
    assert_eq!(parse_current(b"i0e blabla"), (BItem::Integer(0), 3));
    assert_eq!(parse_current(b"i-0e blabla"), (BItem::Integer(0), 4));
    assert_eq!(parse_current(b"i123 e"), (BItem::None, 0));

    assert_eq!(parse_current(b"3:hoi blabla"), (BItem::Bytes(b"hoi".to_vec()), 5));
    assert_eq!(parse_current(b"2:hoi blabla"), (BItem::Bytes(b"ho".to_vec()), 4));
    assert_eq!(parse_current(b"100:a"), (BItem::None, 0));

    assert_eq!(parse_current(b"li32e3:hoie"), (BItem::List(vec![BItem::Integer(32), BItem::Bytes(b"hoi".to_vec())]), 11));
    assert_eq!(parse_current(b"le"), (BItem::List(Vec::new()), 2));
    assert_eq!(parse_current(b"llee"), (BItem::List(vec![BItem::List(Vec::new())]), 4));
    assert_eq!(parse_current(b"l e"), (BItem::None, 0));

    parse_current(b"li32");
    parse_current(b"di32");

    parse_current(b"d2:idi32e");

    assert_eq!(
        parse_current(b"d2:idi1e4:name5:marcke"), 
            (BItem::Dict(vec![
                (b"id".to_vec(), BItem::Integer(1)),
                (b"name".to_vec(), BItem::Bytes(b"marck".to_vec()))
            ])
         , 22)
    );

    let parsed = parse_current(b"d2:idi32ee");
    match parsed {
        (BItem::Dict(d), _) => {
            assert_eq!(find(d.as_slice(), b"id"), Some(&BItem::Integer(32)));
        },
        _ => panic!("Parsed is not a dictionary!")
    }
}
