extern crate bencode;

use bencode::decode;
use bencode::BItem;

fn main() {
    assert_eq!(decode(b"d2:idi1e4:name5:marcke"),
    Some(
        vec![BItem::Dict(
            vec![
                (b"id".to_vec(), BItem::Integer(1)),
                (b"name".to_vec(), BItem::Bytes(b"marck".to_vec()))
            ])
        ]
    ));
}
